" use 256 colors in vim (not gvim) executed in terminal (xfce4-terminal)
set t_Co=256
"set gfn=Monospace\ 8
set guifont=Liberation\ Mono\ for\ Powerline\ 8

" show numbering
set nu

" use relative numbers
set rnu

" some optimizations
set ttyfast
set lazyredraw

" set theme
set background=dark
colorscheme hybrid_reverse
"hi NonText ctermbg=NONE
"hi Normal guibg=NONE ctermbg=NONE

" highlight omnicompletion or every popup menu with custom colors
highlight Pmenu ctermfg=2 ctermbg=3 guifg=#333333 guibg=#9d9d9d

"set textwidth=79  " lines longer than 79 columns will be broken
"set shiftwidth=4  " operation >> indents 4 columns; << unindents 4 columns
set tabstop=4     " a hard TAB displays as 4 columns
set expandtab     " insert spaces when hitting TABs
set softtabstop=4 " insert/delete 4 spaces when hitting a TAB/BACKSPACE
set fileformat=unix
set shiftwidth=0    " Use tabstop

autocmd FileType python,c :setlocal sw=4 ts=4 sts=4 " Two spaces for HTML files "
autocmd FileType javascript,html,css :setlocal sw=2 ts=2 sts=2 " Two spaces for HTML files "

" Artistic stle trigger for gq shortcut
autocmd FileType c set formatprg=astyle\ -A3p

" PERSISTENT UNDO
if has('persistent_undo')      "check if your vim version supports it
  set undofile                 "turn on the feature  
  set undodir=$HOME/.vim/undo  "directory where the undo files will be stored
  set undolevels=5000
  endif     


" Autocommand for CSS HTML & PHP
au InsertLeave *.css :w | :BLReloadCSS
"au InsertLeave *.css :BLReloadCSS
au BufWritePost *.php :BLReloadPage

" when auto-split window always below/right
set splitbelow
set splitright

" modify backspace wierd behaviour
set backspace=indent,eol,start

" map leaders
:let mapleader=";"
:let maplocalleader="'"

" turn on highlight search:
set hlsearch

" automatic folding when open file
set foldclose=all
set foldnestmax=1
let c_no_comment_fold = 1

function! ToggleFoldingMethods()
    if (&foldmethod == "manual")
        :set foldmethod=syntax
    else
        :set foldmethod=manual
    endif
endfunction

nmap <leader>fm :call ToggleFoldingMethods()<CR>

" using mouse to navigate:
set mouse=a

" put+ to <leader>p
nmap <Leader>p :put+<CR>

" insert semicolon automatically when press ; and ENTER in INSERT mode
inoremap ;<cr> <end>;<end>

" Quickfix window always on the bottom
:autocmd FileType qf wincmd J

" nerdtree shortcut:
map <Leader><TAB> :NERDTreeToggle<CR>
let NERDTreeMapActivateNode='<space>'

"Search in project custom function:
function SearchMyProject()
    if (&ft == "c")
        let filetype = ".[hc]"
    elseif (&ft == "cpp")
        let filetype = ".[hc]"
    elseif (&ft == "python")
        let filetype = ".py"
    elseif (&ft == "sh")
        let filetype = ".sh"
    else
        let filetype = ""
    endif

    if ( $VIMPRJ_PROJECT_ROOT != "" )
        :execute "vimgrep /" . expand("<cword>") . "/j " . expand($VIMPRJ_PROJECT_ROOT) . "/**/*" . expand(filetype) | cw
    else
        if ( filetype == "" )
            :execute "vimgrep /" . expand("<cword>") . "/gj " . expand("%") | cw
        else
            :execute "vimgrep /" . expand("<cword>") . "/j *" . expand(filetype) | cw
        endif
    endif
endfunction

"Search across files:
nnoremap <C-f> :call SearchMyProject()<CR>
nnoremap <Leader>k :cn<CR>
nnoremap <Leader>j :cp<CR>
"because I don’t use <C-f> for page down ever, I use <C-u> and <C-d> which does about
"half a page at a time. So I can have the cursor on a word, hit <C-f> and it’ll search
"the project for me, and open the ‘quickfix’ list with the results, then I can use
"<Leader>j and <Leader>k to go to next / previous hit.

" buffers switching
function! BufSel(pattern)
  let bufcount = bufnr("$")
  let currbufnr = 1
  let nummatches = 0
  let firstmatchingbufnr = 0
  while currbufnr <= bufcount
    if(bufexists(currbufnr))
      let currbufname = bufname(currbufnr)
      if(match(currbufname, a:pattern) > -1)
        echo currbufnr . ": ". bufname(currbufnr)
        let nummatches += 1
        let firstmatchingbufnr = currbufnr
      endif
    endif
    let currbufnr = currbufnr + 1
  endwhile
  if(nummatches == 1)
    execute ":buffer ". firstmatchingbufnr
  elseif(nummatches > 1)
    let desiredbufnr = input("Enter buffer number: ")
    if(strlen(desiredbufnr) != 0)
      execute ":buffer ". desiredbufnr
    endif
  else
    echo "No matching buffers"
  endif
endfunction

"Bind the BufSel() function to a user-command
command! -nargs=1 Bs :call BufSel("<args>")
nnoremap <Leader>v :Bs<space>

" easy split switching
nmap <Leader><Space> <c-w>w

" easy buffer switching
nmap <Space> :b#<cr>
nnoremap <Leader>m :bn<cr>
nnoremap <Leader>n :bp<cr>
nnoremap <Leader>d :bp<bar>bd#<cr>
nnoremap <Leader>b :buffers<CR>:buffer<space>

" easy windows resizing
nnoremap <silent> + :exe "vertical resize " . (winwidth(0) * 3/2)<CR>
nnoremap <silent> - :exe "vertical resize " . (winwidth(0) * 2/3)<CR>

" easy panes navigation DISABLED for Seamless TMUX mode
"nnoremap <a-j> <c-w>j
"nnoremap <a-k> <c-w>k
"nnoremap <a-h> <c-w>h
"nnoremap <a-l> <c-w>l

" close current split
nnoremap <Leader>q <c-w>q

" -------------------------------------------
" TMUX VIM Seamless navigation without Plugin
" -------------------------------------------
"function! TmuxMove(direction)
"        let wnr = winnr()
"        silent! execute 'wincmd ' . a:direction
"        " If the winnr is still the same after we moved, it is the last pane
"        if wnr == winnr()
"                call system('tmux select-pane -' . tr(a:direction, 'phjkl', 'lLDUR'))
"        end
"endfunction
"
"nnoremap <silent> <C-h> :call TmuxMove('h')<cr>
"nnoremap <silent> <C-j> :call TmuxMove('j')<cr>
"nnoremap <silent> <C-k> :call TmuxMove('k')<cr>
"nnoremap <silent> <C-l> :call TmuxMove('l')<cr>

" ------------------
" TMUX VIM Seamless 
" ------------------
"  For some reason to make C-Space working, we can keep the default bindings
let g:tmux_navigator_save_on_switch = 2

"Map Space
"inoremap <C-Space> <C-w>w
"inoremap <C-@> <C-Space>

" ------
" VUNDLE
" ------
set nocompatible    " be iMproved, required by vundle
filetype off        " required by vundle

set laststatus=2    " reqired by airline

" disable panel to have more space for text
"set guioptions=0
"set guioptions+=r
set guioptions=Ace

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
" call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Keep Plugin commands between vundle#begin/end.
"
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
"Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
"Plugin 'user/L9', {'name': 'newL9'}

" Tmux Seamless mode

Plugin 'christoomey/vim-tmux-navigator'

" chage surrounding
Plugin 'surround.vim'

" Inteligent autocompletion for quotes, parenthesis, bracket, etc.
Plugin 'git://github.com/Raimondi/delimitMate.git'

" Tree like file browser on left side
Plugin 'scrooloose/nerdtree.git'

" Status line on bottom
Plugin 'bling/vim-airline'

" Show indentation in files
Plugin 'Yggdroot/indentLine'

" Show git changes in left column by +/=/~
Plugin 'airblade/vim-gitgutter'

" show branches
"Plugin 'tpope/vim-fugitive'
Plugin 'itchyny/vim-gitbranch'

" fuzzy buffer switching
Plugin 'ctrlpvim/ctrlp.vim'

" Drawing plugin
Plugin 'https://github.com/vim-scripts/DrawIt'

" syntax checking plugin
"Plugin 'https://github.com/scrooloose/syntastic'

" Avoid message and open already opened file
Plugin 'gioele/vim-autoswap'

" Apply command to visualy selected region
Plugin 'vis'

" Create tables
Plugin 'dhruvasagar/vim-table-mode'

" Operate with multiple coursors
Plugin 'https://github.com/terryma/vim-multiple-cursors.git'

" Orgmode
Plugin 'jceb/vim-orgmode'
Plugin 'tpope/vim-speeddating'

" Suite to work with projects from D.Frank
"Plugin 'indexer.tar.gz'
"Plugin 'vimprj'
"Plugin 'DfrankUtil'

" if you use Vundle, load plugins:
Bundle 'ervandew/supertab'
Bundle 'SirVer/ultisnips'
" various snippets for ultisnips
Plugin 'honza/vim-snippets'

" HTML tags
Plugin 'alvan/vim-closetag'

"automatically save sessions
Plugin 'tpope/vim-obsession'

"join lines
Plugin 'sk1418/Join'

" Tabular
Plugin 'godlygeek/tabular'

" auto resizing windows
Plugin 'roman/golden-ratio'

" last edited files
Plugin 'yegappan/mru'

" Airline Themes
Plugin 'vim-airline/vim-airline-themes'

" CSS Omnicomplete
Plugin 'othree/csscomplete.vim'

"python checker plugin
Plugin 'python-mode/python-mode'

" Autopopup omnicomplete menu
Plugin 'vim-scripts/AutoComplPop'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
filetype plugin on
syntax on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" better key bindings for UltiSnipsExpandTrigger
" complete snippet with TAB
let g:UltiSnipsExpandTrigger = "<tab>"
" jump to next item in snippet by TAB
let g:UltiSnipsJumpForwardTrigger = "<tab>"
" show autocompletion with Shift+TAB
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" IndentLine faster
"let g:indentLine_faster = 1

" Gitgutter settings
let g:gitgutter_realtime = 0
let g:gitgutter_eager = 0

" airline settings
:let g:airline_theme="hybrid"
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#hunks#enabled = 0
let g:airline#extensions#tabline#buffers_label = ''

let g:airline#extensions#tabline#buffer_idx_mode = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9

let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline_section_b = '%{gitbranch#name()}'
let g:airline_section_c = '%f'
let g:airline_section_y = '%{ObsessionStatus("[ACTIVE]", "[PAUSED]")}'

"" Disable HTML/tidy errors
"let g:syntastic_html_tidy_ignore_errors=[" proprietary attribute "]
""let g:syntastic_html_tidy_ignore_errors=[" proprietary attribute " ,"trimming empty <", "unescaped &" , "lacks \"action", "is not recognized!", "discarding unexpected"]


" persistent undo
if has('persistent_undo')      "check if your vim version supports it
  set undofile                 "turn on the feature
  set undodir=$HOME/.vim/undo  "directory where the undo files will be stored
  endif

" Spellcheck
nmap  ;s     :set invspell spelllang=en<CR>
nmap  ;ss    :set    spell spelllang=sk<CR>
" add autocompletion from dictionary
set complete+=k
" To create the en-basic (or any other new) spelling list:
"
"     :mkspell  ~/.vim/spell/en-basic  basic_english_words.txt
"
" See :help mkspell


" Solve conflict delimitmate with closetags when adding additional >
let g:closetag_filenames = "*.xml,*.html,*.xhtml,*.phtml,*.php"
au FileType xml,html,phtml,php,xhtml,js let b:delimitMate_matchpairs = "(:),[:],{:}"

" change indent color
let g:indentLine_color_term = 239

" disable golden ratio for nerdTree
let g:golden_ratio_exclude_nonmodifiable = 1
let g:golden_ratio_autocommand = 0

" toggle relative / non-relative numbering
nmap <leader>r :set rnu!<CR>

" toggle golden ratio
nmap <leader>gr :GoldenRatioToggle<CR>

" use silver searcher insted of grep
if executable('ag')
  " Use Ag over Grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif

" Set up a keymapping from <Leader>df to a function call.
" (Note the function doesn't need to be defined beforehand.)
" Run this mapping silently. That is, when I call this mapping,
" don't bother showing "call DiffToggle()" on the command line.
nnoremap <silent> <Leader>df :call DiffToggle()<CR>

" Define a function called DiffToggle.
" The ! overwrites any existing definition by this name.
function! DiffToggle()
    " Test the setting 'diff', to see if it's on or off.
    " (Any :set option can be tested with &name.
    " See :help expr-option.)
    if &diff
        diffoff
    else
        windo diffthis
    endif
:endfunction

" html snippets in php files
au BufRead *.php set ft=php.html
au BufNewFile *.php set ft=php.html

"CSS Omni Complete Function
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS noci
autocmd FileType css,scss setlocal iskeyword=@,48-57,_,-,?,!,192-255

" enable python3 in python mode
let g:pymode_python = 'python3'
" speed up pythonmode
let g:pymode_folding = 0

" Toggle Wraps
function ToggleWrap()
 if (&wrap == 1)
   set nowrap
 else
   set wrap
 endif
endfunction
map <F9> :call ToggleWrap()<CR>
map! <F9> ^[:call ToggleWrap()<CR>

" for kitty
let &t_ut=''

" nvim on mac fix
if has("nvim")
    " for neovim
    let g:python2_host_prog = '/usr/local/bin/python'
    let g:python3_host_prog = '/usr/local/bin/python3'
endif

" MACVIM fix
if has("gui_macvim")
  if has('python3')
      command! -nargs=1 Py py3 <args>
      set pythonthreedll=/usr/local/Frameworks/Python.framework/Versions/3.6/Python
      set pythonthreehome=/usr/local/Frameworks/Python.framework/Versions/3.6
  else
      command! -nargs=1 Py py <args>
      set pythondll=/usr/local/Frameworks/Python.framework/Versions/2.7/Python
      set pythonhome=/usr/local/Frameworks/Python.framework/Versions/2.7
  endif
endif

" Better hihlight for focused window
augroup BgHighlight
    autocmd!
    autocmd WinEnter * set colorcolumn=80
    autocmd WinLeave * set colorcolumn=0
augroup END
