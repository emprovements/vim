#!/bin/bash

if [[ -d "$HOME/.vim" ]]; then
    echo ".vim folder exists, renaming to .vim.old"
    mv $HOME/.vim $HOME/.vim.old
fi
if [[ -e "$HOME/.vimrc" ]]; then
    echo ".vimrc file exists, renaming to .vimrc.old"
    mv $HOME/.vimrc $HOME/.vimrc.old
fi

mv $HOME/vim $HOME/.vim
ln -s $HOME/.vim/vimrc $HOME/.vimrc

mkdir -p $HOME/.vim/budle

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
git clone --recursive https://github.com/python-mode/python-mode ~/.vim/bundle/pyhton-mode

if [[ "$OSTYPE" == "linux-gnu" ]]; then
    echo "Installing powerline fonts..."
    cd $HOME/.vim
    git clone https://github.com/powerline/fonts.git --depth=1
    # install
    cd fonts
    ./install.sh
    # clean-up a bit
    rm -rf fonts
fi


tmux_installed=$(command -v tmux)
if [[ -z "$tmux_installed" ]]; then
    echo "Install TMUX !!!"
fi

echo "Deploying tmux config..."
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
if [[ -e "$HOME/.tmux.conf" ]]; then
    echo ".tmux.conf file exists, renaming to .tmux.conf.old"
    mv $HOME/.tmux.conf $HOME/.tmux.conf.old
fi
ln -s $HOME/.vim/tmux.conf $HOME/.tmux.conf

echo ""
echo "Open VIM and run :PluginInstall"
echo "Open TMUX and run Prefix - I "
echo "Optionally install zsh prezto and fzf"
echo ""
