#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...

export VISUAL=vim
export EDITOR="$VISUAL"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# -------------------------------------------------------------------
# FIXES
# -------------------------------------------------------------------

# python fix
export LC_ALL=en_US.UTF-8


# -------------------------------------------------------------------
# PATHS
# -------------------------------------------------------------------

# for MOC
path=('/usr/local/Cellar/moc/2.5.0-beta1/bin' $path )

# for my bin's
path=('/Users/mg/bin' $path )

# for MacVim
path=('/Applications/MacVim.app/Contents/bin' $path )

# Metasploit
#PATH=$PATH:/opt/metasploit-framework/bin
export PATH=$PATH:/opt/metasploit-framework/bin


# -------------------------------------------------------------------
# HOOKS
# -------------------------------------------------------------------

# hook to cd
function chpwd() {
  clear
  ls
}

# -------------------------------------------------------------------
# ALIAS
# -------------------------------------------------------------------

# HIDDEN ALIASES
alias cat=ccat

# -------------------------------
# Git
# -------------------------------
#alias ga='git add'
#alias gp='git push'
#alias gl='git log'
#alias gs='git status'
#alias gd='git diff'
#alias gm='git commit -m'
#alias gma='git commit -am'
#alias gb='git branch'
#alias gc='git checkout'
#alias gra='git remote add'
#alias grr='git remote rm'
#alias gpu='git pull'
#alias gcl='git clone'
#alias gta='git tag -a -m'
#alias gf='git reflog'
## leverage an alias from the ~/.gitconfig
#alias gh='git hist'

# PUBLIC ALIASES

alias anaconda='export PATH=/Users/mg/anaconda3/bin:$PATH'

alias s='du -hsc * | sort -h'

alias psgrep="ps -fax | grep -v grep | grep -C1 $1"

alias moon="curl 'wttr.in/moon'; echo"
alias wttr="curl 'wttr.in/?0'; echo"
alias weather="curl 'wttr.in/?n'; echo"

alias cal="ncal -w -A2 -C"
alias clock="tty-clock -c"

alias hl="highlight $1 -O ansi"

alias ic="kitty icat $1"

alias yo="open -a yoink"

alias jn="anaconda; jupyter notebook"

alias a-scan="sudo arp-scan --localnet"

# -------------------------------------------------------------------
# FUNCTIONS
# -------------------------------------------------------------------

function mkcd() {
  case "$1" in
    */..|*/../) cd -- "$1";; # that doesn't make any sense unless the directory already exists
    /*/../*) (cd "${1%/../*}/.." && mkdir -p "./${1##*/../}") && cd -- "$1";;
    /*) mkdir -p "$1" && cd "$1";;
    */../*) (cd "./${1%/../*}/.." && mkdir -p "./${1##*/../}") && cd "./$1";;
    ../*) (cd .. && mkdir -p "${1#.}") && cd "$1";;
    *) mkdir -p "./$1" && cd "./$1";;
  esac
}


function transfer() {
  curl --upload-file $1 https://transfer.sh/$(basename $1);
}


function f() {
  if [[ -z "$2" ]]; then
    find . -iname "*$1*" | grep -ih "$1"
  else
    find . -iname "*$1*$2" | grep -ih "$1"
  fi
}


function grp() {
  if [[ -z "$2" ]]; then
    grep --exclude-dir=.vimprj --exclude-dir=.git -rn . -e "$1"
  else
    find . -name "*.$2" -exec grep --color=auto -H -n -w -i -e "$1" {} \;
  fi
}


function loc() {
  if [[ -z "$2" ]]; then
    locate -i "$1" | grep --color=never $(pwd) | grep -i "$1"
  else
    locate -i "$1" | grep --color=never $(pwd) | grep -i -E "$1[_a-zA-z0-9\-]*\.$2$"
  fi
}


function randpwd() { </dev/urandom tr -dc '12345!@#$%qwertQWERTasdfgASDFGzxcvbZXCVB' | head -c8; echo;}
function randpwd2() { cat /dev/urandom | tr -Cd a-zA-Z0-9 | fold -w 16 | head -1;}


function diffc() {
  find . -name '*.[ch]' -exec diff -wibu {} $1/{} \;
}


function diffcq() {
  find . -name '*.[ch]' -exec diff -wibuq {} $1/{} \;
}


function dirdiffq() {
    diff --brief -r $1 $2
}

function dirdiff() {
    diff --brief -Nr $1 $2
}

FMT="%C(yellow)%h %C(blue)%s%C(auto)%w(0,0,7)%+d%n %an \
%ai%x08%x08%x08%x08%x08%x08%x08%x08%x08 %C(bold)(%ar%x08%x08%x08%x08)%C(reset) \
%n"
alias log="git log --all --pretty='$FMT' --graph"

# -------------------------------------------------------------------
# HELP
# -------------------------------------------------------------------
function help(){
    clear
    echo
    echo "  > diffcq    file                :search file and do the quiet diff"
    echo "  > diffc     file                :search file and do the diff"
    echo "  > s                             :print size of folders anf files"
    echo "  > transfer  file                :upload file to transfer.sh"
    echo "  > psgrep    process             :grep for process in ps"
    echo "  > randpwd                       :generate random phrase"
    echo "  > randpwd2                      :generate random phrase"
    echo "  > anaconda                      :activate anaconda env"
    echo "  > mkcd      dir_name            :mkdir and cd into it"
    echo "  > hl        file                :highlight file"
    echo "  > g         word  [extension]   :grep for word"
    echo "  > loc       file  [extension]   :locate file"
    echo "  > f         file  [extension]   :find file"
    echo "  > moon                          :wttr curl moon"
    echo "  > wttr                          :wttr curl one day"
    echo "  > weather                       :wttr curl long"
    echo "  > cal                           :calendar"
    echo "  > clock                         :tty-clock -c"
    echo "  > dirdiffq  dir dir             :diff for two dirs short"
    echo "  > dirdiff   dir dir             :diff for two dirs"
    echo "  > ic        file                :draw file in terminal"
    echo "  > yo        file                :yoink"
    echo "  > jn                            :source conda, start jupyter notebook"
    echo "  > a-scan                        :arp-scan"
    echo "  > log                           :git log"
    echo
}


# -------------------------------------------------------------------
# FZF
# -------------------------------------------------------------------
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
setopt GLOB_COMPLETE

